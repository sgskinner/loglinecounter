package edu.arizona.katts.config;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;



public class ApplicationProperties{

    private static PropertiesConfiguration propsConfig;
    static{
        try{
            propsConfig = new PropertiesConfiguration("counter.properties");
        }catch(ConfigurationException e){
            throw new RuntimeException(e);
        }
    }


    public static BigDecimal getBigDecimal(final String arg0, final BigDecimal arg1){
        return propsConfig.getBigDecimal(arg0, arg1);
    }


    public static BigDecimal getBigDecimal(final String key){
        return propsConfig.getBigDecimal(key);
    }


    public static BigInteger getBigInteger(final String arg0, final BigInteger arg1){
        return propsConfig.getBigInteger(arg0, arg1);
    }


    public static BigInteger getBigInteger(final String key){
        return propsConfig.getBigInteger(key);
    }


    public static boolean getBoolean(final String key, final boolean defaultValue){
        return propsConfig.getBoolean(key, defaultValue);
    }


    public static Boolean getBoolean(final String arg0, final Boolean arg1){
        return propsConfig.getBoolean(arg0, arg1);
    }


    public static boolean getBoolean(final String key){
        return propsConfig.getBoolean(key);
    }


    public static byte getByte(final String key, final byte defaultValue){
        return propsConfig.getByte(key, defaultValue);
    }


    public static Byte getByte(final String arg0, final Byte arg1){
        return propsConfig.getByte(arg0, arg1);
    }


    public static byte getByte(final String key){
        return propsConfig.getByte(key);
    }


    public static double getDouble(final String key, final double defaultValue){
        return propsConfig.getDouble(key, defaultValue);
    }


    public static Double getDouble(final String arg0, final Double arg1){
        return propsConfig.getDouble(arg0, arg1);
    }


    public static double getDouble(final String key){
        return propsConfig.getDouble(key);
    }


    public static float getFloat(final String key, final float defaultValue){
        return propsConfig.getFloat(key, defaultValue);
    }


    public static Float getFloat(final String arg0, final Float arg1){
        return propsConfig.getFloat(arg0, arg1);
    }


    public static float getFloat(final String key){
        return propsConfig.getFloat(key);
    }


    public static int getInt(final String key, final int defaultValue){
        return propsConfig.getInt(key, defaultValue);
    }


    public static int getInt(final String key){
        return propsConfig.getInt(key);
    }


    public static Integer getInteger(final String arg0, final Integer arg1){
        return propsConfig.getInteger(arg0, arg1);
    }


    @SuppressWarnings({"rawtypes", "unchecked"})
    public static List getList(final String arg0, final List arg1){
        return propsConfig.getList(arg0, arg1);
    }


    public static List<String> getList(final String key){
        List<Object> tmpList = propsConfig.getList(key);
        List<String> newList = new LinkedList<String>();
        for(Object o : tmpList){
            newList.add((String)o);
        }
        return newList;
    }


    public static long getLong(final String key, final long defaultValue){
        return propsConfig.getLong(key, defaultValue);
    }


    public static Long getLong(final String arg0, final Long arg1){
        return propsConfig.getLong(arg0, arg1);
    }


    public static long getLong(final String key){
        return propsConfig.getLong(key);
    }


    public static short getShort(final String key, final short defaultValue){
        return propsConfig.getShort(key, defaultValue);
    }


    public static Short getShort(final String arg0, final Short arg1){
        return propsConfig.getShort(arg0, arg1);
    }


    public static short getShort(final String key){
        return propsConfig.getShort(key);
    }


    public static String getString(final String key, final String defaultValue){
        return propsConfig.getString(key, defaultValue);
    }


    public static String getString(final String key){
        return propsConfig.getString(key);
    }


    public static String[] getStringArray(final String arg0){
        return propsConfig.getStringArray(arg0);
    }


    public static Map<String, String> getMap(final String propertyKey){
        String line = propsConfig.getString(propertyKey);
        String[] entryStrings = line.split(";");
        Map<String, String> resultMap = new HashMap<String, String>();
        for(String entryString : entryStrings){
            String[] pair = entryString.split("=");
            String key = pair[0];
            String value = pair[1];
            key = key.trim();
            value = value.trim();
            resultMap.put(key, value);
        }
        return resultMap;
    }


    public static TimeUnit getTimeUnit(final String key){
        String val = getString(key);
        if(key == null){
            return null;
        }
        return TimeUnit.valueOf(val.toUpperCase());
    }


    public static Set<String> getSet(final String key){
        String[] stringArray = propsConfig.getStringArray(key);
        Set<String> resultSet = new TreeSet<String>();
        for(String value : stringArray){
            value = value.trim();
            resultSet.add(value);
        }
        return resultSet;
    }


    public static void load(final String filePath){
        try{
            propsConfig.load(filePath);
        }catch(ConfigurationException e){
            throw new RuntimeException(e);
        }
    }


    public static void load(final File file){
        try{
            propsConfig.load(file);
        }catch(ConfigurationException e){
            throw new RuntimeException(e);
        }
    }

}
