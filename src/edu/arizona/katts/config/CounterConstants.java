package edu.arizona.katts.config;

import java.util.regex.Pattern;

/**
 * Values will be picked up from PROJECT/config/counter.properties, except for
 * patterns, which are set here.
 */
public class CounterConstants {
	
	// Load our properties file
	private static final String PROPERTIES_FILE_NAME = "counter.properties";
	static {
		ApplicationProperties.load(PROPERTIES_FILE_NAME); 
	}
	
	// Pattern used to match Java class names
	public static final Pattern CLASS_NAME_PATTERN = Pattern.compile("(\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*\\.)+\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}+");
	
	// Stack Track Element pattern, used to skip lines in a stack trace
	public static final Pattern STE_PATTERN = Pattern.compile("^\\s+|^Caused by");
	
	// File to read in and count
	public static final String LOG_FILE_PATH = ApplicationProperties.getString("input.log.file.path");

	// Whether to exclusions to stdout or not
	public static final boolean PRINT_EXLUSIONS_TO_STDOUT = ApplicationProperties.getBoolean("report.excluded.lines");

	// Whether to report matching counts to file, if so where
	public static final boolean WRITE_COUNTS_FILE_OUT = ApplicationProperties.getBoolean("write.counts.out");
	public static final String COUNTS_FILE_PATH = ApplicationProperties.getString("counts.file.path");

	// Whether to report exclusions to file, if so where
	public static final boolean WRITE_EXLUSION_FILE_OUT = ApplicationProperties.getBoolean("write.exlusions.out");
	public static final String EXLUSION_FILE_PATH = ApplicationProperties.getString("exclusions.file.path");

}
