package edu.arizona.katts.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;



public class FileUtils {

    public static List<String> getLines(final String filename) {
        List<String> lines = new LinkedList<String>();
        LineIterator lineIter = getLineIterator(filename);
        while(lineIter.hasNext()){
            lines.add(lineIter.next());
        }
        return lines;
    }


    public static void writeToFile(final String fileName, final String value) {
        Writer writer = getWriter(new File(fileName));
        try{
            writer.write(value);
            writer.flush();
            IOUtils.closeQuietly(writer);
        }catch(IOException e){
            throw new RuntimeException(e);
        }
    }


    public static void writeLine(final String string, final Writer writer, final boolean flush) {
        try{
            IOUtils.write(string, writer);
            if(flush){
                writer.flush();
            }
        }catch(IOException e){
            IOUtils.closeQuietly(writer);
            throw new RuntimeException(e);
        }
    }


    public static LineIterator getLineIterator(final String fileName) {
        return getLineIterator(new File(fileName));
    }


    public static LineIterator getLineIterator(final File file) {
        Reader reader = getReader(file);
        return IOUtils.lineIterator(reader);
    }

    public static Writer getWriter(final String filePath) {
    	File file = new File(filePath);
    	return getWriter(file);
    }

    public static Writer getWriter(final File file) {
        Writer writer = null;
        try{
            writer = new FileWriter(file);
        }catch(Exception e){
            IOUtils.closeQuietly(writer);
            throw new RuntimeException(e);
        }
        return writer;
    }
    
    public static void flushWriter(Writer writer) {
    	try {
			writer.flush();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
    }


    public static Reader getReader(final File file) {
        Reader reader = null;
        try{
            reader = new FileReader(file);
        }catch(FileNotFoundException e){
            throw new RuntimeException(e);
        }
        return reader;
    }


    public static void writeCollectionToFile(final String filename, final Collection<String> strings) {
        String newLine = String.format("%n");
        Writer writer;
        try{
            writer = new FileWriter(new File(filename));
        }catch(IOException e){
            throw new RuntimeException(e);
        }

        try{
            IOUtils.writeLines(strings, newLine, writer);
            writer.flush();
        }catch(IOException e){
            throw new RuntimeException(e);
        }finally{
            IOUtils.closeQuietly(writer);
        }
    }
}
