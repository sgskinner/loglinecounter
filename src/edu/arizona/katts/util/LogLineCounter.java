package edu.arizona.katts.util;

import java.io.Writer;
import java.text.NumberFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;

import edu.arizona.katts.config.CounterConstants;

public class LogLineCounter {

	// Set these in $PROJECT/config/counter.properties
	private static final String INPUT_LOG_PATH = CounterConstants.LOG_FILE_PATH;
	private static final Pattern CLASS_NAME_PATTERN = CounterConstants.CLASS_NAME_PATTERN;
	private static final Pattern STE_PATTERN = CounterConstants.STE_PATTERN;



	/*
	 * All this just to sort by keys; gives us am easier to read when reporting
	 */
	public static <K,V extends Comparable<? super V>> SortedSet<Map.Entry<K,V>> entriesSortedByValues(Map<K,V> map) {
	    SortedSet<Map.Entry<K,V>> sortedEntries = new TreeSet<Map.Entry<K,V>>(
	        new Comparator<Map.Entry<K,V>>() {
	            @Override
	            public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
	                int res = e1.getValue().compareTo(e2.getValue());
	                return res != 0 ? res : 1;
	            }
	        }
	    );
	    sortedEntries.addAll(map.entrySet());
	    return sortedEntries;
	}


	public static void main(final String[] args) {

		// Bookkeeping variables 
		Map<String, Integer> classToCountMap = new HashMap<String, Integer>();
		int totalLinesCounted = 0;

		// Setup in the case we will be writing files for counts or exclusions
		Writer countWriter = null;
		Writer exclusionWriter = null;
		if(CounterConstants.WRITE_COUNTS_FILE_OUT){
			countWriter = FileUtils.getWriter(CounterConstants.COUNTS_FILE_PATH);
			exclusionWriter = FileUtils.getWriter(CounterConstants.EXLUSION_FILE_PATH);
		}

		/*
		 * Algo:
		 * 1. Get line iterator for log file
		 * 2. Walk line by line
		 * 3. Skip lines that are STE details (indented, "caused by", etc)
		 * 4. Match regex for class name pattern
		 * 5. Increment a counter for each unique class
		 * 6. Report to stdout each class name found, along with occurence count for each
		 * 7. Report final aggregate stats
		 */
		LineIterator lineIter = FileUtils.getLineIterator(INPUT_LOG_PATH);
		while (lineIter.hasNext()) {

			String line = lineIter.next();
			totalLinesCounted++;

			// Need to short circuit stack traces
			Matcher ste_matcher = STE_PATTERN.matcher(line);
			if(ste_matcher.find()){
				continue;
			}

			// Match non-ste lines
			Matcher matcher = CLASS_NAME_PATTERN.matcher(line);
			if (matcher.find()) {
				String className = matcher.group(0);
				Integer count = classToCountMap.get(className);
				if (count == null) {
					count = 0;
				}
				count++;
				classToCountMap.put(className, count);
			} else {

				// Case this is an exclusion,no java class name found

				if (CounterConstants.PRINT_EXLUSIONS_TO_STDOUT) {
					System.out.printf("--EXCLUDED--> %s%n", line);
				}

				if(CounterConstants.WRITE_EXLUSION_FILE_OUT){
					String withNewline = String.format("%s%n", line);
					FileUtils.writeLine(withNewline, exclusionWriter, false);
				}
			}

		} // while

		// Sort entries so we can report in order of smallest to largest counts
		SortedSet<Entry<String, Integer>> sortedEntrySet = entriesSortedByValues(classToCountMap);

		// Actual reporting
		int numLinesMatched = 0;
		for (Entry<String, Integer> entry : sortedEntrySet) {
			int count = entry.getValue();
			numLinesMatched += count;

			String statLine = String.format("%6d: %s%n", count, entry.getKey());
			
			// Always write matches to stdout
			System.out.printf(statLine);

			// Write to file if enabled
			if(CounterConstants.WRITE_COUNTS_FILE_OUT){
				FileUtils.writeLine(statLine, countWriter, false);
			}
		}

		// Final stats
		NumberFormat numFormatter = NumberFormat.getInstance();
		int numLinesExcluded = totalLinesCounted - numLinesMatched;
		String lineSepar = String.format("--------------------------------------\n");
		String totalCount = String.format("Total lines processed: %12s\n", numFormatter.format(totalLinesCounted));
		String excludedCount = String.format("   Num Lines excluded: %12s\n", numFormatter.format(numLinesExcluded));
		String matchedCount = String.format("    Num lines matched: %12s\n", numFormatter.format(numLinesMatched));
		String uniqueCount = String.format("      Num unique hits: %12s\n", numFormatter.format(sortedEntrySet.size()));

		// To stdout
		System.out.printf(lineSepar);
		System.out.printf(totalCount);
		System.out.printf(excludedCount);
		System.out.printf(matchedCount);
		System.out.printf(uniqueCount);

		// Write counts if config says to, also cleanup writer
		if(CounterConstants.WRITE_COUNTS_FILE_OUT){
			FileUtils.writeLine(lineSepar, countWriter, false);
			FileUtils.writeLine(totalCount, countWriter, false);
			FileUtils.writeLine(excludedCount, countWriter, false);
			FileUtils.writeLine(matchedCount, countWriter, false);
			FileUtils.writeLine(uniqueCount, countWriter, false);
			FileUtils.flushWriter(countWriter);
			IOUtils.closeQuietly(countWriter);
		}

		// Final clean-up for exclustions writer
		if(CounterConstants.WRITE_EXLUSION_FILE_OUT){
			FileUtils.flushWriter(exclusionWriter);
			IOUtils.closeQuietly(exclusionWriter);
		}

	}

}
